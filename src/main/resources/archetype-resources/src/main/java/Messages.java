#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.opencms.i18n.A_CmsMessageBundle;
import org.opencms.i18n.I_CmsMessageBundle;

public class Messages extends A_CmsMessageBundle {

	private static final String BUNDLE_NAME = "${package}.workplace";

	private static final I_CmsMessageBundle INSTANCE = new Messages();

	private Messages() {

		// hide the constructor
	}

	public static I_CmsMessageBundle get() {

		return INSTANCE;
	}

	public String getBundleName() {

		return BUNDLE_NAME;
	}
}